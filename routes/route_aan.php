<?php

use App\Models\GajiPegawai;
use App\Models\KelompokGaji;
use App\Models\KelompokGajiDetail;
use App\User;
use Illuminate\Http\Request;

// param id or all
Route::get('gaji/pegawai/{id}',function($id){

	$data['pageTitle'] = 'gaji pegawai';
	if($id != 'all'){
		$data['pegawai'] = GajiPegawai::where('id_user',$id)->with('pegawai','kelompokGaji.childreen.parentKomponen')->get();
	}else{
		$data['pegawai'] = GajiPegawai::with('pegawai','kelompokGaji.childreen.parentKomponen')->get();
	}

	return view('gajipegawai',$data);
});

// tampilans
Route::get('gaji/format',function(){

	$data['pageTitle'] = 'format gaji';
	$data['kelompokGaji'] = \DB::table('kelompok_gaji')->pluck('nama','id');

	return view('formatgaji',$data);
});

Route::get('gaji/get-form',function(Request $request){
	$form = KelompokGaji::with(['childreen.parentKomponen'])->find($request->id);

	$data['status'] = 'success';
	$data['data'] = $form->toArray();

	return  response()->json($data,200);
})->name('gaji.get.form');	