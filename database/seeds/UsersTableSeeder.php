<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::statement('ALTER TABLE users AUTO_INCREMENT = 1');

        $users = array([
            'name'  => 'Aan Dhimas',
            'email' => 'yo.dhimas@gmail.com',
            'password' => bcrypt('02188976458'),
        ],[
            'name'  => 'Agustianto',
            'email' => 'agustiant9129@gmail.com',
            'password' => bcrypt('Output4321'),
        ]);

        DB::table('users')->insert($users);
    }
}
