<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="{{ asset('limitless_assets/global_assets/images/placeholders/placeholder.jpg') }}" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold">{{ Auth::user()->name }} </div>
                        <div class="font-size-xs opacity-50">
                            <!-- <i class="icon-pin font-size-sm"></i> &nbsp;Santa Ana, CA -->
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                        <!-- <a href="#" class="text-white"><i class="icon-cog3"></i></a> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Status navbar aktif/tidak aktif -->

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item">
                    <a href="{{route('home')}}" class="nav-link @if(Route::is('home')) active @endif">
                        <i class="icon-home4"></i><span>Dashboard<!-- <span class="d-block font-weight-normal opacity-50">No active orders</span> --></span>
                    </a>
                </li>
                
                <li class="nav-item">
                    <a href="{{ route('pegawai.index') }}" class="nav-link @if(Route::is('pegawai.index')) active @endif"><i class="icon-circle-right2"></i><span>Pegawai</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('gaji.index') }}" class="nav-link @if(Route::is('gaji.index')) active @endif"><i class="icon-circle-right2"></i><span>Gaji Pegawai</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('gaji.komponen.index') }}" class="nav-link @if(Route::is('gaji.komponen.index')) active @endif"><i class="icon-circle-right2"></i><span>Komponen Gaji</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('departements.index') }}" class="nav-link @if(Route::is('departements.index')) active @endif"><i class="icon-circle-right2"></i><span>Departemen</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('user.index') }}" class="nav-link @if(Route::is('user.index')) active @endif"><i class="icon-user"></i><span>User</span></a>
                </li>
                
                <!-- /main -->

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->
    
</div>