@section('content')
<!-- Page header -->
<div class="page-header page-header-light">

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                <a href="user_pages_list.html" class="breadcrumb-item">User pages</a>
                <span class="breadcrumb-item active">User list</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">

        </div>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Simple lists -->
    <div class="row">
        <div class="col-md-12">

            <!-- Simple list -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Simple user list</h5>
                    <div class="header-elements">
                        <div class="list-icons">

                        </div>
                    </div>
                </div>

                <div class="card-body">

                </div>
            </div>
            <!-- /simple list -->

        </div>


    </div>
    <!-- /simple lists -->
</div>
<!-- /content area -->

@endsection