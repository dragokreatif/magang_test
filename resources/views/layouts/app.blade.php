<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('limitless_assets/global_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('limitless_assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('limitless_assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('limitless_assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('limitless_assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('limitless_assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>

<body>

    <!-- Main navbar -->
    @include('layouts.navbar-top')
    <!-- /main navbar -->


    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        @include('layouts.sidebar')
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">
            <!-- page header -->
            <div class="page-header page-header-light">

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                            <!-- <a href="user_pages_list.html" class="breadcrumb-item">User pages</a> -->
                            <span class="breadcrumb-item active">{{$pageTitle}}</span>
                        </div>

                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>

                    <div class="header-elements d-none">

                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            @yield('content')
            <!-- /Content area -->

            <!-- Footer -->
            @include('layouts.footer')
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

    <!-- Core JS files -->
    <script src="{{ asset('limitless_assets/global_assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('limitless_assets/global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('limitless_assets/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{ asset('limitless_assets/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('limitless_assets/global_assets/js/demo_pages/form_layouts.js') }}"></script>
    <script src="{{ asset('limitless_assets/global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script src="{{ asset('limitless_assets/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    
    <script src="{{ asset('limitless_assets/global_assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <!-- <script src="{{ asset('limitless_assetsglobal_assets/js/plugins/visualization/d3/d3_tooltip.js') }} "></script> -->
    <script src="{{ asset('limitless_assets/global_assets/js/plugins/forms/styling/switchery.min.js') }} "></script>
    <!-- <script src="../../../../global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script> -->
    <script src="{{ asset('limitless_assets/global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('limitless_assets/global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>

    <script src="{{ asset('limitless_assets/js/app.js') }}"></script>
    <script src="{{ asset('limitless_assets/global_assets/js/demo_pages/dashboard.js') }}"></script>
    <script src="{{ asset('limitless_assets/global_assets/js/demo_pages/form_select2.js') }}"></script>
    <!-- /theme JS files -->

    <script type="text/javascript">
        // push notify
        session = '{!! Session::get('flash_message') !!}';
        delay = 2500;

        if(session == 'success'){

            p = new PNotify({
                title: 'Success',
                text: 'Success',
                icon: 'icon-check2',
                type: 'success',
                delay: delay
            });

        }else if(session == 'error'){
            p = new PNotify({
                title: 'Danger',
                text: 'Terjadi kesalahan',
                icon: 'icon-blocked',
                type: 'error',
            });
        }

        $('.del').on('click',function(e){
            e.preventDefault();
            a = confirm('apakah akan menghapus data ?');
            
            if(a){
                link = $(this).attr('href');
                window.location.assign(link);
            }

        });
    </script>
    @stack('scripts')
    <!-- to add scritpt use push('scripts') -->
</body>
</html>
