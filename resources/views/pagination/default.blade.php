@if ($paginator->hasPages())

    <ul class="pagination wbs-default justify-content-center">
        @if ($paginator->onFirstPage())
        <li class="page-item prev disabled"><a href="#" class="page-link">Prev</a></li>
        @else
        <li class="page-item"><a href="{{ $paginator->previousPageUrl() }}" class="page-link">Prev</a></li>
        @endif

        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><span class="page-link">{{ $page }}</span></li>
                    @else
                        <li class="page-item"><a href="{{ $url }}" class="page-link">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
        
        @if ($paginator->hasMorePages())
            <li class="page-item next"><a href="{{ $paginator->nextPageUrl() }}" class="page-link">Next</a></li>
        @else
            <li class="page-item next disabled"><a href="#" class="page-link">Next</a></li>
        @endif
    </ul>
@endif
