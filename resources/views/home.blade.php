@extends('layouts.app')

@section('content')

<!-- Content area -->
<div class="content">

    <!-- Simple lists -->
    <div class="row">
        <div class="col-md-12">

            <!-- Simple list -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Sample Page</h5>
                    <div class="header-elements">
                        <div class="list-icons">

                        </div>
                    </div>
                </div>

                <div class="card-body">

                </div>
            </div>
            <!-- /simple list -->

        </div>

    </div>
    <!-- /simple lists -->
</div>
<!-- /content area -->
@endsection

<!-- script di sini -->
@push('scripts')
<script type="text/javascript">
    
</script>
@endpush