@extends('layouts.app')

@section('content')

<!-- Content area -->
<div class="content">

    <!-- Simple lists -->
    <div class="row">
        <div class="col-md-12">

            <!-- Basic layout-->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{{$pageTitle}}</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{route('user.store')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <fieldset>
                            <legend class="font-weight-semibold text-uppercase font-size-sm">Enter user information</legend>
                            {{ Session::get('flash_message') }}
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label"> user name</label>
                                <div class="col-lg-9">
                                    <input type="text" name="name" class="form-control" placeholder="...">
                                    @if ($errors->has('name'))
                                    <span class="form-text text-danger">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label"> user password</label>
                                <div class="col-lg-9">
                                    <input type="password" name="password" class="form-control" placeholder="password">
                                </div>
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend class="font-weight-semibold text-uppercase font-size-sm">Add personal details</legend>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label"> user email</label>
                                <div class="col-lg-9">
                                    <input type="text" name="email" class="form-control" placeholder="...">
                                    @if ($errors->has('email'))
                                    <span class="form-text text-danger">{{$errors->first('email')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">User Type</label>
                                <div class="col-lg-9">
                                    @forelse($userType as $key => $value)
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input type="radio" name="type" class="form-input-styled" value="{{$value}}">
                                            {{$value}}
                                        </label>
                                    </div>
                                    @empty
                                    @endforelse
                                    @if ($errors->has('type'))
                                    <span class="form-text text-danger">{{$errors->first('type')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label">foto</label>
                                <div class="col-lg-9">
                                    <input type="file" class="form-input-styled" data-fouc>
                                </div>
                            </div>
                        </fieldset>

                        

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /basic layout -->

        </div>

    </div>
    <!-- /simple lists -->
</div>
<!-- /content area -->

@endsection

<!-- script di sini -->
@push('scripts')
<script type="text/javascript">
    
</script>
@endpush