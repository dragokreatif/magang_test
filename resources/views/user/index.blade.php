@extends('layouts.app')

@section('content')

<!-- Content area -->
<div class="content">

    <!-- Simple lists -->
    <div class="row">
        <div class="col-md-12">

            <!-- Simple list -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{{$pageTitle}}</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a href="{{route($route_create)}}" class="btn btn-success btn-xs ">new user</a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table" border="0">
                        <thead>
                            <tr>
                                <th width="20px">#</th>
                                <th>Name</th>
                                <th width="100px">action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = ($dataUser->currentPage() - 1) * $dataUser->perPage() + 1; @endphp
                            @forelse($dataUser as $key => $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->name}}</td>
                                <td>
                                    <a href="{{route($route_edit,$value->id)}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                                    <a href="{{route($route_destroy,$value->id)}}" class="list-icons-item text-danger-600 del"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                    

                </div>

                <div class="card-footer">
                    <!-- navigation with limitless css -->
                    {{ $dataUser->links('pagination.default') }}
                </div>
            </div>
            <!-- /simple list -->

        </div>


    </div>
    <!-- /simple lists -->
</div>
<!-- /content area -->

@endsection

<!-- script di sini -->
@push('scripts')
<script type="text/javascript">
    
</script>
@endpush