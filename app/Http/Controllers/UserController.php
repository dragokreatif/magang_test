<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;

class UserController extends Controller
{
    public $pageTitle = 'User';
    public $route_index = 'user.index';
    public $route_create = 'user.create';
    public $route_store = 'user.store';
    public $route_edit = 'user.edit';
    public $route_update = 'user.update';
    public $route_destroy = 'user.destroy';
    public $userType = ['admin','salesman','customer'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pageTitle'] = 'User';
        $data['route_create'] = $this->route_create;
        $data['route_edit'] = $this->route_edit;
        $data['route_destroy'] = $this->route_destroy;
        $data['dataUser'] = User::paginate(10);

        return view('user.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['pageTitle'] = 'Create New User';
        $data['route_store'] = $this->route_store;
        $data['route_index'] = $this->route_index;
        $data['userType'] = $this->userType;

        return view('user.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
            'email' => 'unique:users',
            'type' => 'required', 
        ]);

        if($validator->fails()){
           
            return redirect()->back()->withErrors($validator);
        }

        $dtIns = $request->except('_token');
        $dtIns['password'] = bcrypt($request->password);

        \DB::beginTransaction();
        try {
            $user = User::create($dtIns);

            \DB::commit();
            $status = 'success';

            return redirect()->route($this->route_index)->with('flash_message', $status);
        } catch (Exception $e) {
            \DB::rollback();
            $status = 'error';

            return redirect()->route($this->route_create)->with('flash_message', $status);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pageTitle'] = 'Edit User';
        $data['route_update'] = $this->route_update;
        $data['route_index'] = $this->route_index;
        $data['userType'] = $this->userType;
        $data['dtUser'] = User::findorfail($id);
        // dd($data['dtUser']->roles);
        return view('user.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
            // 'email' => 'unique:users',
            'email'=>'required|email|unique:users,email,'.$request->id,
            'komunitas' => 'required', 
        ]);

        if($validator->fails()){
           
            return redirect()->back()->withErrors($validator);
        }
  
        $user = User::findOrFail($request->id);

        $dtIns = $request->except('_token','roles');
        $dtIns['password'] = bcrypt($request->password);
        
        $roles = $request['roles'];

        \DB::beginTransaction();
        try {
            $user->fill($dtIns);

            if (isset($roles)) {

                foreach ($roles as $role) {
                    $role_r = Role::where('id', '=', $role)->firstOrFail();            
                    $user->assignRole($role_r);
                }
            } 

            \DB::commit();
            $status = 'success';

            return redirect()->route($this->route_index)->with('flash_message', $status);
        } catch (Exception $e) {
            \DB::rollback();
            $status = 'error';

            return redirect()->route($route_create)->with('flash_message', $status);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        \DB::beginTransaction();
        try {
            User::findorfail($id)->delete();

            \DB::commit();
            $status = 'success';

            return redirect()->route($this->route_index)->with('flash_message', $status);
        } catch (Exception $e) {
            \DB::rollback();
            $status = 'error';

            return redirect()->route($this->route_index)->with('flash_message', $status);
        }
    }
}
